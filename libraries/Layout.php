<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Layout
{
    private $CI;
    private $var = array();
    private $theme = 'default';
     
/*
|===============================================================================
| Constructeur
|===============================================================================
*/
     
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->var['output'] = '';

        // page title
        $this->var['title'] = ucfirst($this->CI->router->fetch_method()) . ' - ' . ucfirst($this->CI->router->fetch_class());

        // Is home
        $this->var['is_home'] = FALSE;

        // charset
        $this->var['charset'] = $this->CI->config->item('charset');

        $this->var['css'] = array();
        $this->var['js'] = array();
    }
     
    /*
    |===============================================================================
    | Méthodes pour charger les vues
    |   . view
    |   . views
    |===============================================================================
    */
     
    public function view($name, $data = array())
    {
         
        $this->var['output'] .= $this->CI->load->view($name, $data, true);
         
        $this->CI->load->view('../themes/' . $this->theme, $this->var);
    }
     
    public function views($name, $data = array())
    {
        $this->var['output'] .= $this->CI->load->view($name, $data, true);
        return $this;         
    }

    /*
    |===============================================================================
    | Méthodes pour envoyer les variables au layout
    |   . set_titre
    |   . set_charset
    |===============================================================================
    */

    public function set_titre($titre)
    {   
        if(is_string($titre) && !empty($titre)) {
            $this->var['title'] = $titre;
            return true;
        }
        return false;
    }

    public function set_charset($charset)
    {
        if(is_string($charset) && !empty($charset)) {
            $this->var['charset'] = $charset;
            return true;
        }
        return false;
    }

    /*
    |===============================================================================
    | Méthodes pour ajouter des feuilles de CSS et de JavaScript
    |   . add_css
    |   . add_js
    |===============================================================================
    */
    public function add_css($nom)
    {
        if(is_string($nom) AND !empty($nom) AND file_exists('./statics/css/' . $nom . '.css'))
        {
            $this->var['css'][] = css_url($nom);
            return true;
        }
        return false;
    }
     
    public function add_js($nom)
    {        
        if(is_string($nom) AND !empty($nom) AND file_exists('./statics/js/' . $nom . '.js'))
        {
            $this->var['js'][] = js_url($nom);
            return true;
        }
        return false;
    }

    /*
    |===============================================================================
    | Méthodes pour changer de theme
    |   . set_theme    
    |===============================================================================
    */

    public function set_theme($theme)
    {   
        if(is_string($theme) && !empty($theme)) {
            $this->theme = $theme;
            return true;
        }
        return false;
    }

    /*
    |===============================================================================
    | Flag is home
    |   . set_theme    
    |===============================================================================
    */

    public function set_is_home($is_home = FALSE)
    {           
        $this->var['is_home'] = $is_home;        
    }
}
 
/* End of file layout.php */
/* Location: ./application/libraries/layout.php */