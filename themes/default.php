<?php $is_home = (isset($is_home))? $is_home : FALSE; ?>
<!DOCTYPE html>
<html lang='fr'>
    <head>
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $charset; ?>" />
        <meta name="description" content=""/>

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300' rel='stylesheet' type='text/css'>

        <!-- css -->
        <?php foreach ($css as $url): ?>
        	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $url; ?>" />
        <?php endforeach; ?>
        <link rel="stylesheet" href="<?php echo site_url() ?>statics/css/themes/default/default.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo site_url() ?>statics/css/nivo-slider.css" type="text/css" media="screen" />
        <!-- js -->
        <?php foreach($js as $js_url): ?>
		    <script type="text/javascript" src="<?php echo $js_url; ?>"></script> 
		<?php endforeach; ?>
        
        <?php if($is_home) { ?>
            
        <?php } ?>
    </head> 
    <body>
        <div id="header">
            <div id="inner_header" class="full_width centered">
                <div id="logo" class="site_title white_color left">Data - Cutter Marketing</div></a></a>
                <div id="menu" class="left">
                    <?php //echo Modules::run('page/simple_menu') ?>
                    <ul>
                        <li><a href="<?php echo site_url() ?>">Accueil</a></li> 
                        <li><a href="<?php echo site_url() ?>ads/submit">Utilisateurs</li>
                        <li><a href="<?php echo site_url() ?>page/offres-premiums">Se déconnecter</a><li>
                    </ul>        
                </div>
                <div class="clear_left"></div>                
            </div>            
        </div>        
        <div id="content" class="full_width centered">
            <div id="left_content" class="left"><?php echo $output; ?></div>
            <?php echo Modules::run( 'sidebar' ); ?>
            <div class="clear_left"></div>
        </div>
        <div id="footer">
            <div id="info_corporate">
            </div>
            &copy Cutter Marketing 2017 - Tous droits r&eacute;serv&eacute;s
        </div>
    </body>
</html>