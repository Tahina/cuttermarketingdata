<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" > 
    <meta charset="UTF-8">
    <head>
        <title><?php echo $title; ?></title>
        <!-- css -->
        <?php foreach ($css as $url): ?>
        	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $url; ?>" />
        <?php endforeach; ?>        
    </head> 
    <body>
        <div id="contenu">
            <?php echo $output; ?>
        </div>
    </body> 
</html>