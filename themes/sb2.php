<?php $is_home = (isset($is_home))? $is_home : FALSE; ?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="<?php echo $charset; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url() ?>">Data - Cutter Marketing</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-file-text fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> Profil</a> -->
                        </li>
                        <li><a href="<?php echo site_url() ?>companies/import"><i class="fa fa-plus fa-fw"></i> Importer des compagnies</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url() ?>companies/list_companies"><i class="fa fa-th-list fa-fw"></i> Voir toutes les compagnies</a>
                        </li>                        
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user-md fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> Profil</a> -->
                        </li>
                        <li><a href="<?php echo site_url() ?>user/create"><i class="fa fa-plus fa-fw"></i> Ajouter un utilisateur</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url() ?>user/list_users"><i class="fa fa-th-list fa-fw"></i> Voir tous les utilisateurs</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-gear fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> Profil</a> -->
                        </li>
                        <li><a href="<?php echo site_url() ?>user/profile"><i class="fa fa-gear fa-fw"></i> Mon compte</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url() ?>auth/logout"><i class="fa fa-sign-out fa-fw"></i> Se d&eacute;connecter</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <!-- <ul class="nav" id="side-menu">
                        <li class="sidebar-search"> -->
                            <!-- <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Rechercher...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div> -->
                            <!-- /input-group -->
                        <!-- </li> -->
                        <!-- <li>
                            <a href="index.html"><i class="fa fa-gear fa-fw"></i> Filtrer les recherche par</a>
                        </li> -->
                        <!-- <li> -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Outil de recherche
                                </div>
                                <div class="panel-body">
                                    <form name="search" method="get" action="<?php echo site_url() ?>companies/search">
                                        <input type="text" name="s" class="form-control" placeholder="Nom /Raison social/Siret">
                                        <br />
                                        <label>Secteur d'activit&eacute; </label>
                                        <br />
                                        <select>
                                            <option>Secteur 1</option>
                                            <option>Secteur 2</option>
                                            <option>Secteur 3</option>
                                                            
                                        </select>
                                        <br /><br />

                                        <label>Effectif </label>
                                        <br />
                                        <select>
                                            <option>1 - 10</option>
                                            <option>11 - 30</option>
                                            <option>31 - 100</option>                 
                                        </select>
                                        <br /><br />
                                        <label>Localisation </label>
                                        <br />
                                        <select>
                                            <option>Paris</option>
                                            <option>Marseille</option>
                                                                
                                        </select>
                                        <br /><br />
                                        <input type="submit" class="btn btn-primary" value="Chercher">
                                    </form>
                                </div>
                                <!-- <div class="panel-footer">
                                    Panel Footer
                                </div> -->
                            </div>
                            
                        <!-- </li>
                        
                    </ul> -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?php echo $title; ?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <?php echo $output; ?>
            <!-- <div class="row">
                <div class="col-lg-8">
                    
                    
                </div> -->
                <!-- /.col-lg-8 -->
                <!-- <div class="col-lg-4">
                    
                </div> -->
                <!-- /.col-lg-4 -->
            <!-- </div> -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!-- <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/dist/js/sb-admin-2.js"></script>

</body>

</html>
