<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Company_model extends MY_Model
{
    // sets table
    protected $table = 'companies';

    /**
     * Search for companies
     *
     * This function retrieves a series of db entries based on criteria.
     *
     * @param string $like Mostly name or siret of the company
     * @param   array   $criteria       A keyed array of criteria key = field name, value = value, key may also contain comparators (=, !=, >, etc..)
     * @param   int     $limit          The max number of entries to grab (0 = no limit)
     * @param   int     $offset         What record number to start grabbing (useful for pagination)
     * @param   array   $order          A keyed array of "order commands" telling how to sort key = field name, value = direction (asc, desc, random)
     * @param   array   $order          A keyed array of like key = field name
     * @access  public
     * @return  mixed   Return Object of results on success, Boolean False on failure
     */
    public function search($fields = "*", $criteria = '', $limit = 0, $offset = 0, $order = '', $like = '') 
    {
        
        // verify the table we are drawing from has been set.
        if (!empty($this->table)) 
        {
            $this->db->from($this->table);
        } 
        else 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: retrieve($criteria, $limit, $offset, $order); Required to select_table before using functions.');
            return FALSE;
        }

        // Select fields
        $this->db->select($fields);

        // Name or siret
        if(strlen(trim($like)) > 0)
        {
            $this->db->like('NOMEN_LONG', $like);
            $this->db->or_like('SIRET', $like);
        }

        // Verify an array has been passed.
        if (is_array($criteria)) 
        {

            if (is_array($order)) 
            {
                foreach ($order as $order_by => $direction) 
                {
                    $this->db->order_by($order_by, $direction);
                }
                unset($order_by, $direction);
            }

            if (!empty($limit)) 
            {
                if (!empty($offset)) 
                {
                    $this->db->limit($limit, $offset);
                } 
                else 
                {
                    $this->db->limit($limit);
                }
            }

            $this->db->where($criteria);            
            $res = $this->db->get();
            //echo $this->db->get_compiled_select();
            //echo $this->db->last_query();
            return $res;
        } 
        else 
        {
            log_message('error', 'Model: Crud; Method: retrieve($criteria, $limit, $offset, $order); Required parameter not set.');
            return FALSE;
        }
    }

    /**
     * count_results
     *
     * This function simply counts ALL entries in the selected DB with a given criteria.
     *
     * @param   array   $criteria    A keyed array with the critera for selecting what entries to edit.
     * @access  public
     * @return  Mixed   Return Integer of resutls on success, Boolean False on failure
     */
    public function count_search_results($fields = 'SIRET', $criteria, $like = '') 
    {
        // verify the table we are drawing from has been set.
        if (empty($this->table)) 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: count_results($criteria); Required to select_table before using functions.');
            return FALSE;
        }
        
        // Select fields
        $this->db->select($fields);

        // Name or siret
        if(strlen(trim($like)) > 0)
        {
            $this->db->like('NOMEN_LONG', $like);
            $this->db->or_like('SIRET', $like);
        }
        $this->db->where($criteria);
        return $this->db->get($this->table)->num_rows();
        //return $this->db->count_all_results();
    }
}

/* End of file client_model.php */
/* Location: ./application/modules/clients/models/client_model.php */