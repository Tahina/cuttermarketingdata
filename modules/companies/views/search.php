<?php if($companies->num_rows() > 0) { ?>
    <?php $i = 1; ?>
	<div class="panel panel-default">
        <div class="panel-heading">
            <?php echo $total_companies ?> compagnie(s) correspondant a votre requête
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Raison sociale</th>
                            <th>SIRET</th>
                            <th>Adresse</th>
                            <th>Région</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($companies->result() as $company) { ?>
                        	<?php $edit_link = site_url() . 'companies/edit/' . $company->SIRET; ?>
	                        <tr>
	                            <td><?php echo $i++; ?></td>
	                            <td><?php echo $company->NOMEN_LONG ?></td>
	                            <td><?php echo $company->SIRET ?></td>
	                            <td><?php echo $company->L4_DECLAREE ?></td>
	                            <td><?php echo $company->LIBVOIE ?></td>
	                        </tr>
	                    <?php } ?>                        
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /panel -->
        
	<ul class="pagination">
        <?php echo $pagination ?>
    </ul>

	<br />
	<br />
	<br />

<?php } else { ?>
	<h4>No companies</h4>
<?php } ?>