<?php if(strlen(trim($message)) > 0): ?>
    <div class="alert alert-danger"><?php echo $message;?></div>
<?php endif; ?>
<?php echo form_open("companies/import");?>
	<p><?php echo form_input($json_url); ?></p>
	<p><input type="submit" name="submit" class="btn btn-primary" value="Importer"></p>
<?php echo form_close();?>
<br /><br />
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-green">
            <div class="panel-heading">
                Companies
            </div>
            <div class="panel-body">
            	<p><b><?php echo $countSuccessCompany ?></b> companies added successfuly</p>
            </div>
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                Table
            </div>
            <div class="panel-body">
                <p>New columns</p>
                <ul>
                	<?php foreach($newColumns as $col) { ?>
	                	<li><?php echo $col ?></li>
	                <?php } ?>	
                </ul>                
            </div>
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="panel panel-red">
            <div class="panel-heading">
                Errors
            </div>
            <div class="panel-body">
                <p><b><?php echo $doublons ?></b> doublons</p>
                <ul>
                    <?php foreach($siretDoublons as $siret) { ?>
                        <li><?php echo $siret ?></li>
                    <?php } ?>  
                </ul>
            </div>
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->