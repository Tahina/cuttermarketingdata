<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Companies extends MX_Controller {

	protected $table = 'companies';

	public function __construct()
    {
        parent::__construct();
        
  		$this->load->library('layout');
        $this->layout->set_theme('sb2');
        $this->load->model('company_model', 'companyManager');

        $redirect = redirect_url(TRUE);

        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the home ad because they must be an administrator to view this
            redirect('auth/login?back=' . $redirect, 'refresh');            
        }
    }


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		redirect('companies/list_companies', 'refresh');
	}

	/**
	 * Import companies. By now via json only
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function import()
	{
		$countSuccessCompany = 0;
		$countFailCompany = 0;
		$countDoublons = 0;
		$arrNewColumns = array();
		$arrSiretDoublons = array();

		$this->load->library('form_validation');

		//validate form input
		$this->form_validation->set_rules('json_url', '"URL json"', 'required');

		if ($this->form_validation->run() == true)
		{

			$jsonObj;
			$jsonUrl = $this->input->post('json_url');
			//$jsonUrl = "https://data.opendatasoft.com/api/records/1.0/search/?dataset=base-sirene%40datanova&q=Transformation+et+conservation+de+la+viande+de+boucherie&facet=depet&facet=libcom&facet=siege&facet=saisonat&facet=libnj&facet=libapen&facet=libtefen&facet=categorie&facet=proden&facet=vmaj1&facet=vmaj2&facet=vmaj3&facet=liborigine&facet=libtca&facet=libreg_new";

			$response = file_get_contents($jsonUrl);

			$jsonOutput = json_decode($response);
			//var_dump($jsonOutput);

			// loop
			//echo sizeof($jsonOutput->records);
			foreach($jsonOutput->records as $record) {
				//echo $record->fields->l4_declaree . '<br />';
				foreach($record->fields as $key => $value) {
					//echo strtoupper($key) . ', ';
					
					// Check/create if necessary
					$newCol = $this->_add_column_if_not_exist(
								$this->table, 
								strtoupper($key), 
								"VARCHAR( 255 ) NULL" 
							);
					if($newCol) $arrNewColumns[] = $newCol;
				
					// Build query array
					// $dataInsert = array(
					// 	strtoupper($key) => $value
					// );
				}
				//echo ">" . $record->fields->siret . "<<br>";
				$checkSiret = $this->_company_exists($record->fields->siret);
				if($checkSiret == '0') {
					if($this->db->insert($this->table, $record->fields)) $countSuccessCompany ++;
					else $countFailCompany;
				}
				else
				{
					$countDoublons ++;
					$arrSiretDoublons[] = $checkSiret;
				}
			}	
	    } //.end form run

	    //set the flash data error message if there is one
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

	    // form
		$data['json_url'] = array(
            'name'  => 'json_url',
            'id'    => 'json_url',
            'type'  => 'text',
            'class'  => 'form-control',
            'placeholder' => 'Entrer une URL json valide'
        );

        // tracking
        $data['countSuccessCompany'] = $countSuccessCompany;
        $data['countFailCompany'] = $countFailCompany;
        $data['newColumns'] = $arrNewColumns;
        $data['doublons'] = $countDoublons;
        $data['siretDoublons'] = $arrSiretDoublons;

        //loads layout
        $this->layout->set_titre("Charger des compagnies");
        $this->layout->view('companies/import', $data);
	}

	/**
     * list companies
     *      
     * Retrieves the list of ads. Displays list for index and list_ads
     *      
     * @access public
     * @param int $page The page number (pagination)
     * @param session $user->id The id of the current logged in user
     * @return resutlset
     *
     */
    public function list_companies($page = 0)
    {       
        // vars
        $limit = 10; // items per page
        $like = "";
        //$search_r = (isset($_GET['ref']))? htmlentities($_GET['ref']) : '';
        $search_s = (isset($_GET['s']))? htmlentities($_GET['s']) : '';
        $search_s = trim($search_s);

        // loads libraries
        // $this->load->model('user_model', 'companyManager');
        // $this->load->library('layout');
        $this->load->library('pagination');
        //$this->load->helper('date_format');
        
        // calculates offset
        $page = (($page - 1) >=0 )? ($page - 1) : 0;
        $offset = $page * $limit;
        $fields = "NOMEN_LONG, SIRET, L4_DECLAREE, LIBVOIE";
        
        // Get the list of Items from the DB
        $criteria = array('1' => 1);    // Any entry that has an ID not equal to zero. 
        //if(!$this->ion_auth->is_admin()) $criteria['user_id'] = $user->id;    // Shows only the ads of the current user
        
        $order_by = array('NOMEN_LONG' => 'ASC');   // order by id asscending
        $query = $this->companyManager->retrieve($fields, $criteria, $limit, $offset, $order_by, $like);

        // pagination
        $this->load->library('pagination');
        $config['base_url']   = site_url() . 'companies/list_companies/';
        $config['total_rows'] = $this->companyManager->count_results($fields, $criteria, $like);
        $config['per_page']   = $limit;    // if you change this you must also change the crud call below.        
        $config['use_page_numbers'] = TRUE;
        $config['prev_link'] = '&lt; Prec';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = 'Suiv &gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
                        
        $data['companies'] = $query;
        $data['total_companies'] = $config['total_rows'];
        
        // load layout
        //$this->layout->set_theme($root_theme . 'default');
        $this->layout->set_titre('Compagnies');
        //$this->layout->add_css('layout');
        //$this->layout->add_css('style');
        //$this->layout->add_js('jquery-1.8.3');
        $this->layout->view('companies/list', $data);

    }

    /**
     * Search companies by name/siret
     *      
     * Retrieves the list of companies.
     *      
     * @access public
     * @param int $page The page number (pagination)
     * @return resutlset res
     *
     */
    public function search($page = 0)
    {       
        // vars
        $limit = 50; // items per page
        $like = "";
        $page = (($page - 1) >=0 )? ($page - 1) : 0;
        $offset = $page * $limit;
        
        // Search term (name or SIRET)
        $search_s = (isset($_GET['s']))? htmlentities($_GET['s']) : '';

        $fields = "NOMEN_LONG, SIRET, L4_DECLAREE, LIBVOIE";
        
        // Get the list of Items from the DB
        $criteria = array();    // Any entry that has an ID not equal to zero. 
        //if(!$this->ion_auth->is_admin()) $criteria['user_id'] = $user->id;    // Shows only the ads of the current user
        
        $order_by = array('NOMEN_LONG' => 'ASC');   // order by id asscending
        $query = $this->companyManager->search($fields, $criteria, $limit, $offset, $order_by, $search_s);

        // pagination
        $this->load->library('pagination');
        $config['base_url']   = site_url() . 'companies/search';
        $config['total_rows'] = $this->companyManager->count_search_results('SIRET', $criteria, $search_s);
        $config['per_page']   = $limit;    // if you change this you must also change the crud call below.        
        $config['use_page_numbers'] = TRUE;
        $config['prev_link'] = '&lt; Prec';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = 'Suiv &gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
                        
        $data['companies'] = $query;
        $data['total_companies'] = $config['total_rows'];
        
        // load layout
        //$this->layout->set_theme($root_theme . 'default');
        $this->layout->set_titre('Résultats de recherche sur "' . $search_s . '"');
        //$this->layout->add_css('layout');
        //$this->layout->add_css('style');
        //$this->layout->add_js('jquery-1.8.3');
        $this->layout->view('companies/search', $data);

    }


	/**
	 * Checks if a column exists in table, if not, adds it
	 */
	function _add_column_if_not_exist($tbl, $column, $column_attr = "VARCHAR( 255 ) NULL"){
	    
	    $arrCols = array();

	    $q = "SELECT `COLUMN_NAME` 
				FROM `INFORMATION_SCHEMA`.`COLUMNS` 
				WHERE `TABLE_SCHEMA`='cuttjjbx_datamining' 
				    AND `TABLE_NAME`='companies'";
	    $resCol = $this->db->query($q);
	    //$cols = $resCol->result_array();
	    //echo $cols[0];
	    foreach($resCol->result() as $value){
	    	$arrCols[] = $value->COLUMN_NAME;
	    }
	    //print_r($arrCols);
	    if(!in_array($column, $arrCols)){
            //echo "ALTER TABLE `$tbl` ADD `$column`  $column_attr <br />";
        	$this->db->query("ALTER TABLE `$tbl` ADD `$column`  $column_attr");
        	return $column;
        }
        return false;
	}

	/**
	 * Checks if a record by SIRET already exists
	 *
	 * @param $id_siret SIRET company's unique id
	 * return boolean true/false
	 */
	function _company_exists($id_siret){
	    $siret = '0';

	    $criteria = array(
	    	'SIRET' => $id_siret
	    );
	    $this->db->select('SIRET');
		$this->db->where($criteria);
        $res = $this->db->get($this->table);
        //var_dump($res);
        if($res->num_rows() != NULL) {
        	
	        foreach ($res->result() as $row) {
	        	$siret = $row->SIRET;
	        }
        }        

        return $siret;
	}
}
