<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	public function __construct()
    {
        $this->load->library('layout');
        $redirect = redirect_url(TRUE);
        $this->layout->set_theme('sb2');

        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the home ad because they must be an administrator to view this
            redirect('auth/login?back=' . $redirect, 'refresh');
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->layout->set_titre('Data - Cutter Marketing');
		// $this->layout->add_css('layout');
		// $this->layout->add_css('style');
		$this->layout->view('welcome');
	}
}
