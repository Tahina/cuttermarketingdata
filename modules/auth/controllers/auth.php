<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
        
  		//$this->load->library('ion_auth');
		$this->load->library('form_validation');
		//$this->layout->set_theme('sb2');
		//$this->load->library('layout');
		//$this->load->helper('url')

		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :

		$this->load->database();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->helper('language');
    }


	//redirect if needed, otherwise display the user list
	function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		// elseif (!$this->ion_auth->is_admin()) to add later
		// {
		// 	//redirect them to the home page because they must be an administrator to view this
		// 	redirect('/', 'refresh');
		// }
		else
		{
			//set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->_render_page('auth/index', $data);
		}
	}

	/* Login */
	function login () {

		$data['title'] = "Login";
		
		/* reCaptcha */
		// Ma clé privée
		$secret = "6LeR9DkUAAAAADw5_mYme1tY9mAUwZiEhIQaAMlv";
		// Paramètre renvoyé par le recaptcha
		$response = (isset($_POST['g-recaptcha-response']))? $_POST['g-recaptcha-response'] : FALSE;
		// On récupère l'IP de l'utilisateur
		$remoteIp = $_SERVER['REMOTE_ADDR'];

		$redirect = redirect_url();

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		/* Check is login action is performed */
		if ($this->form_validation->run() == true)
		{
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			/* recaptcha */
			$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $response . "&remoteip=" . $remoteIp;

			$decode = json_decode(file_get_contents($api_url), true);
			
			if ($decode['success'] === FALSE) {
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', 'You are a robot');
				redirect('auth/login?back=' . $redirect, 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}


			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$redirect = (trim($redirect) != '')? $redirect : '/';
				//echo "Ok" . $redirect;
				redirect($redirect, 'refresh');
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login?back=' . $redirect, 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
				//echo "KO";
			}
		} else {

			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$data['redirect'] = $redirect;

			$data['identity'] = array(
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'email',
				'class' => 'form-control',
				'placeholder' => 'E-mail',
				'value' => $this->form_validation->set_value('identity'),
			);
			$data['password'] = array(
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'class' => 'form-control',
				'placeholder' => 'Mot de passe'
			);

			// $data['btn_connect'] = array(
			// 	'class' => 'btn btn-default',
			// 	'value' => $this->form_validation->set_value('identity'),
			// );

			//$this->_render_page('auth/login', $data); OLD
			// load layout
			//$this->layout->set_theme('admin/defaul');
			// $this->layout->set_titre('Authentification');
			// $this->layout->add_css('layout');
			// $this->layout->add_css('style');
			$this->load->view('auth/login', $data);
			//$this->_render_page('auth/index', $data);
		}
	}

	//log the user out
	function logout()
	{
		$data['title'] = "Logout";

		// adds redirect after logout
		$back_url = (isset($_GET['back']))? htmlentities($_GET['back']) : '/';

		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect($back_url, 'refresh');
	}
}