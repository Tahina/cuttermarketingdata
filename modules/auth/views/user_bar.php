<div id="user_bar">
	<?php //print_r($user) ?>
	<?php if($user != null && $user->id > 0): ?>
		<?php 
		$full_url = current_url();
		$base_url = site_url();
		$back_url = str_replace($base_url, '', $full_url);
		?>
		<?php echo $user->first_name ?> | 
		<a href="<?php echo site_url() ?>user/profile">Modifier mon profil</a> | 
		<a href="<?php echo site_url() ?>user/ads">Mes annonces</a> | 
		<a href="<?php echo site_url() ?>ads/add">Ajouter une annonce</a> | 
		<a href="<?php echo site_url() ?>auth/logout?back=<?php echo $back_url; ?>">Se d&eacute;connecter</a>
	<?php else: ?>
		<a href="<?php echo site_url() ?>user/login">Se connecter</a> | <a href="<?php echo site_url() ?>user/register">S'enregistrer</a>
	<?php endif; ?>
</div>