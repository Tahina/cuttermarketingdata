<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Authentification - Data Cutter Marketing</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo lang('login_heading', 'indentity');?></h3>
                    </div>
                    <div class="panel-body">
                        <?php if(strlen(trim($message)) > 0): ?>
                            <div class="alert alert-danger"><?php echo $message;?></div>
                        <?php endif; ?>
                        <?php echo form_open("user/login?back=$redirect");?>
                            <fieldset>
                                <div class="form-group">
                                    <?php echo form_input($identity); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo form_input($password);?>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?><?php echo lang('login_remember_label', 'remember');?>
                                    </label>
                                </div>
                                <div class="g-recaptcha" data-sitekey="6LeR9DkUAAAAAOxZTL55J6kpw23qx_NGO_rFfajD"></div>
                                <?php //echo form_submit('btn_connect', lang('login_submit_btn'));?>
                                <input type="submit" name="submit" class="btn btn-primary btn-lg" value="Se connecter">
                            </fieldset>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/dist/js/sb-admin-2.js"></script>

</body>
</html>