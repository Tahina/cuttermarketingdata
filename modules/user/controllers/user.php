<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MX_Controller
{
	public function __construct()
    {
        parent::__construct();
        
        $this->load->library('layout');
        $this->layout->set_theme('sb2');
        
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        
        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'ion_auth') ?
        $this->load->library('mongo_db') :

        $this->load->database();

        $redirect = redirect_url(TRUE);
        // if (!$this->ion_auth->is_admin()) add late
        // {
        //     //redirect them to the home ad because they must be an administrator to view this
        //     redirect('auth/login?back=' . $redirect, 'refresh');
        // }
    }

    /**
     * Index 
     *      
     *      
     * @access public
     * @return void
     *
     */    
	public function index()
	{		
        redirect('user/profile', 'refresh');
	}

    /**
     * Profile
     *
     * Edit profile      
     *      
     * @access public
     * @return void
     *
     */    
    public function profile($user_id = 0)
    {       
        
        $this->load->library('form_validation');
        $this->load->model('user_model', 'userManager');
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('user/login', 'refresh');
        } else {            
            // grabs user info
            if(intval($user_id) > 0) {
                $where_id = array('id' => $user_id);
                $user = $this->userManager->get_single($where_id);    
                $data['title'] = "Modifier un profil";    
            }
            else
            {
                $user = $this->ion_auth->user()->row();    
                $data['title'] = "Modifier mon profil";
            }

            
                   
            
            $data['email'] = $user->email;
                        
            $groups = $this->ion_auth->groups()->result_array();
            $currentGroups = $this->ion_auth->get_users_groups($user->id)->result();

            //process the phone number
            if (isset($user->phone) && !empty($user->phone))
            {
                //$user->phone = explode('-', $user->phone);
            }

            //validate form input
            $this->form_validation->set_rules('first_name', '"Pr&eacute;noms"', 'required|min_length[3]|max_length[150]|alpha_numeric');
            $this->form_validation->set_rules('last_name', '"Nom"', 'required|min_length[3]|max_length[150]|alpha_numeric');
            $this->form_validation->set_rules('phone1', '"T&eacute;l&eacute;phone"', 'required|numeric|min_length[10]|max_length[10]');
            //$this->form_validation->set_rules('phone2', $this->lang->line('edit_user_validation_phone2_label'), 'required|xss_clean|min_length[3]|max_length[3]');
            //$this->form_validation->set_rules('phone3', $this->lang->line('edit_user_validation_phone3_label'), 'required|xss_clean|min_length[4]|max_length[4]');
            //$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required|xss_clean');
            $this->form_validation->set_rules('groups', $this->lang->line('edit_user_validation_groups_label'), 'xss_clean');

            if (isset($_POST) && !empty($_POST))
            {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('id'))
                {
                    show_error($this->lang->line('error_csrf'));
                }

                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name'  => $this->input->post('last_name'),
                    'company'    => $this->input->post('company'),
                    'phone'      => $this->input->post('phone1'),
                );

                //Update the groups user belongs to
                $groupData = $this->input->post('groups');

                if (isset($groupData) && !empty($groupData)) {

                    $this->ion_auth->remove_from_group('', $id);

                    foreach ($groupData as $grp) {
                        $this->ion_auth->add_to_group($grp, $id);
                    }

                }

                //update the password if it was posted
                if ($this->input->post('password'))
                {
                    $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                    $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');

                    $data['password'] = $this->input->post('password');
                }

                if ($this->form_validation->run() === TRUE)
                {
                    $this->ion_auth->update($user->id, $data);

                    //check to see if we are creating the user
                    //redirect them back to the admin page
                    $this->session->set_flashdata('message', "Modifications enregistr&eacute;es");
                    
                    redirect("user/profile", 'refresh');
                }
            }

            $data['email'] = $user->email;
            //display the edit user form
            $data['csrf'] = $this->_get_csrf_nonce();

            //set the flash data error message if there is one
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            //pass the user to the view
            $data['user'] = $user;
            $data['groups'] = $groups;
            $data['currentGroups'] = $currentGroups;

            $data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name', $user->first_name),
            );
            $data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name', $user->last_name),
            );
            /*
            $data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company', $user->company),
            );
            */
            $data['phone1'] = array(
                'name'  => 'phone1',
                'id'    => 'phone1',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone1', $user->phone),
            );
            /*
            $data['phone2'] = array(
                'name'  => 'phone2',
                'id'    => 'phone2',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone2', $user->phone[1]),
            );
            $data['phone3'] = array(
                'name'  => 'phone3',
                'id'    => 'phone3',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone3', $user->phone[2]),
            );
            */
            $data['password'] = array(
                'name' => 'password',
                'id'   => 'password',
                'type' => 'password'
            );
            $data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id'   => 'password_confirm',
                'type' => 'password'
            );    
            /* end */

            //loads layout
            $this->load->library('layout');            
            $this->layout->set_titre("Modifier mon profil");
            //$this->layout->add_css('layout');
            //$this->layout->add_css('style');
            //$this->layout->add_js('jquery-1.8.3');
            $this->layout->view('user/profile', $data);
        }
    }


    /**
     * Register
     *
     * Register a new user
     *      
     * @access public
     * @return void
     *
     */    
    public function create()
    {       
        
        $this->load->library('form_validation');
        $this->load->helper('recaptchalib');
        //$this->load->config();

        /* Loads recaptcha keys from config file */
        $private_key = $this->config->item('recaptcha_private_key');
        $public_key = $this->config->item('recaptcha_public_key');
        
        //validate form input
        $this->form_validation->set_rules('first_name', '"Pr&eacute;noms"', 'required|min_length[3]|max_length[150]|alpha_numeric');
            $this->form_validation->set_rules('last_name', '"Nom"', 'required|min_length[3]|max_length[150]|alpha_numeric');
            $this->form_validation->set_rules('phone1', '"T&eacute;l&eacute;phone"', 'required|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('email', '<b><i>E-mail</i></b>', 'required|valid_email');
        
        //$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'required|xss_clean');
        $this->form_validation->set_rules('password', '<b><i>Mot de passe</i></b>', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', '<b><i>Confirmation mot de passe</i></b>', 'required');
        //$this->form_validation->set_rules('recaptcha_response_field', '<b><i>Image de confirmation</b></i>', 'required|xss_clean');

        if ($this->form_validation->run($this) == TRUE)
        {
            
            // receives data
            $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
            $email    = $this->input->post('email');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'company'    => '',
                'phone'      => $this->input->post('phone1')
            );

            /* recaptcha */            
            // $resp_captcha = recaptcha_check_answer ($private_key,
            //             $_SERVER["REMOTE_ADDR"],
            //             $_POST["recaptcha_challenge_field"],
            //             $_POST["recaptcha_response_field"]);

            //echo '>>>' . $resp_captcha->is_valid;
                        

        } // if run 1
        
        if ($this->form_validation->run($this) == TRUE && $this->ion_auth->register($username, $password, $email, $additional_data))
        {            
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("user/message", 'refresh');            
        } 
        else
        {
            //display the create user form
            //set the flash data error message if there is one
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $data['phone1'] = array(
                'name'  => 'phone1',
                'id'    => 'phone1',
                'type'  => 'text',
                'maxlength'  => '10',
                'value' => $this->form_validation->set_value('phone1'),
            );            
            $data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            //$data['recaptcha_public_key'] = $public_key;

            //loads layout
            $this->load->library('layout');            
            $this->layout->set_titre("Ajouter un nouvel utilisateur");
            $this->layout->add_css('layout');
            $this->layout->add_css('style');
            $this->layout->add_js('jquery-1.8.3');
            $this->layout->view('user/create', $data);
        } // if run 2
    }

    
    //forgot password
    function forgot_password()
    {
        //setup the input
        $data['email'] = array('name' => 'email',
            'id' => 'email',
        );

        //set any errors and display the form
        $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        
        //loads layout
        $this->load->library('layout');            
        $this->layout->set_titre("Mot de passe oubli&eacute;");
        $this->layout->add_css('layout');
        $this->layout->add_css('style');
        $this->layout->view('user/forgot_password', $data);
    }

    /**
     * message
     *      
     *      
     * @access public
     * @return void
     *
     */    
    public function message()
    {       
        //set the flash data error message if there is one
        $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

        //loads layout
        $this->load->library('layout');            
        $this->layout->set_titre("Message");
        $this->layout->add_css('layout');
        $this->layout->add_css('style');
        $this->layout->view('user/message', $data);
    }

    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key   = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /* Validates recaptcha */
    function _valide_recaptcha($response = '')
    {
        //$this->load->helper('recaptchalib');

        /*
        $private_key = '6Leu5OUSAAAAAEvrMiWIJpxUw4nGO3gZaGJ4k_4f';
        $public_key = '6Leu5OUSAAAAAAtAbYDVxRWWeh-PwQAX8qzNJKKB';

        
        $resp_captcha = recaptcha_check_answer ($private_key,
                        $_SERVER["REMOTE_ADDR"],
                        $_POST["recaptcha_challenge_field"],
                        $_POST["recaptcha_response_field"]);

        if(!$resp_captcha->is_valid)
        {
            $this->form_validation->set_message('_valide_recaptcha', 'Les deux mots de confirmation que vous avez entr&eacute;s ne sont pas valides');
            return FALSE;
        } else {
            return TRUE;
        }
        */
    }        

    /* Validates recaptcha */
    function _check_first_name($new_name = '')
    {
        if($new_name != 'Tahina')
        {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_first_name', 'First name cannot be Tahina');
            return FALSE;
        }
    }

    /**
     * list_ad 
     *      
     * Retrieves the list of ads. Displays list for index and list_ads
     *      
     * @access public
     * @param int $page The page number (pagination)
     * @param session $user->id The id of the current logged in user
     * @return resutlset
     *
     */
    public function list_users($page = 0)
    {       
        // vars
        $limit = 10; // items per page
        $like = "";
        //$search_r = (isset($_GET['ref']))? htmlentities($_GET['ref']) : '';
        $search_s = (isset($_GET['s']))? htmlentities($_GET['s']) : '';

        // loads libraries
        $this->load->model('user_model', 'userManager');
        $this->load->library('layout');
        $this->load->library('pagination');
        //$this->load->helper('date_format');
        
        // calculates offset
        $page = (($page - 1) >=0 )? ($page - 1) : 0;
        $offset = $page * $limit;
        $fields = '*';
        
        // Get the list of Items from the DB
        $criteria = array('id !=' => 0);    // Any entry that has an ID not equal to zero. 
        //if(!$this->ion_auth->is_admin()) $criteria['user_id'] = $user->id;    // Shows only the ads of the current user
        
        $order_by = array('id' => 'ASC');   // order by id asscending
        $query = $this->userManager->retrieve($fields, $criteria, $limit, $offset, $order_by, $like);

        // pagination
        $this->load->library('pagination');
        //$config['base_url']   = site_url($list_url);
        $config['total_rows'] = $this->userManager->count_results($criteria, $like);
        $config['per_page']   = $limit;    // if you change this you must also change the crud call below.        
        $config['use_page_numbers'] = TRUE;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
                        
        $data['users'] = $query;
        
        // load layout
        //$this->layout->set_theme($root_theme . 'default');
        $this->layout->set_titre('Utilisateurs');
        //$this->layout->add_css('layout');
        //$this->layout->add_css('style');
        //$this->layout->add_js('jquery-1.8.3');
        $this->layout->view('user/list', $data);
    }
}

/* End of file user.php */
/* Location: ./application/modules/user/controllers/user.php */