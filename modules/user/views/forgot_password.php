<h2>Mot de passe oubli&eacute;</h2>

<p>Entrez votre adresse email pour que nous puissions vous aider à réinitialiser votre mot de passe</p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("user/forgot_password");?>

      <p>
      	<label for="email">Email valide</label> <br />
      	<?php echo form_input($email);?>
      </p>

      <p><?php echo form_submit('submit', 'Réinitialiser');?></p>

<?php echo form_close();?>