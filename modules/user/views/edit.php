<!-- <div class="page_title">Mon compte <span class="small_title"> <span class="bc_separator">></span> Modifier <span class="highlighted">mon</span> profil</span></div> -->

<?php if(trim($message) != '') { ?>
      <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $message;?>
      </div>
<?php } ?>

<?php echo form_open(uri_string());?>

      
                  <table>
                        <tr>
                              <td style="padding: 10px" valign="top">
                                    <div class="panel panel-default">
                                          <div class="panel-heading">
                                                Identification
                                          </div>
                                          <div class="panel-body">
                                                <label>E-mail</label>
                                                <div class="disabled_input"><?php echo $email ?></div>
                                                <br />
                                                <label>Mot de passe</label><br />
                                                <?php echo form_input($password);?>
                                                <br /><br />
                                                <label>Confirmer mot de passe</label><br />
                                                <?php echo form_input($password_confirm);?>                        
                                          </div>
                                    </div> 
                              </td>
                              <td style="padding: 10px" valign="top">
                                    <div class="panel panel-default">
                                          <div class="panel-heading">
                                                Infos personnelles
                                          </div>
                                          <div class="panel-body">     
                                                <label>Prénoms</label><br />
                                                <?php echo form_input($first_name);?>
                                                <br /><br />
                                                <label>nom</label><br />
                                                <?php echo form_input($last_name);?>
                                                <br /><br />
                                                <label>Telephone</label><br />
                                                <?php echo form_input($phone1);?>
                                          </div>
                                    </div>
                              </td>
                              
                        </tr>
                  </table>
                  


      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <p>
            <input type="submit" name="submit" class="btn btn-primary" value="Enregistrer les modifications">
            <?php //echo form_submit('submit', 'Enregistrer les modifications');?>
                  
      </p>

<?php echo form_close();?>