<?php 
function add_column_if_not_exist($db, $column, $column_attr = "VARCHAR( 255 ) NULL" ){
    $exists = false;
    $columns = mysql_query("show columns from $db");
    while($c = mysql_fetch_assoc($columns)){
        if($c['Field'] == $column){
            $exists = true;
            break;
        }
    }      
    if(!$exists){
        mysql_query("ALTER TABLE `$db` ADD `$column`  $column_attr");
    }
}

/* End of file mysql_helper.php */
/* Location: ./application/helpers/mysql_helper.php */