<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( ! function_exists('site_url'))
{
    function site_url($uri = '')
    {       
        if( ! is_array($uri))
        {
            //  Tous les paramètres sont insérés dans un tableau
            $uri = func_get_args();
        }
     
        //  On ne modifie rien ici
        $CI =& get_instance();  
        return $CI->config->site_url($uri);
    }
}
 
// ------------------------------------------------------------------------
 
if ( ! function_exists('url'))
{
    function url($text, $uri = '')
    {
        if( ! is_array($uri))
        {
            //  Suppression de la variable $text
            $uri = func_get_args();
            array_shift($uri);
        }
     
        echo '<a href="' . site_url($uri) . '">' . htmlentities($text) . '</a>';
        return '';
    }
}

/*
Retrieves $_GET['back'] from current url and returns it
*/
if ( ! function_exists('redirect_url'))
{
    function redirect_url($is_admin = FALSE)
    {
        if($is_admin) 
        {
            /* admin */
            $current_url = uri_string();
            $redirect = $current_url;
        } else {
            /* front-end */
            $s = 1;
            $params = array();
            $current_url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
            $url_arr = explode('?', $current_url);
            //echo uri_string();
            foreach ($url_arr as $key => $value) {
                if($s > 1) $params[] = $value;
                $s++;
            }

            $redirect = join('?', $params);
            $redirect = str_replace('back=', '', $redirect);
        }

        return $redirect;
    }
}
 
 
/* End of file MY_url_helper.php */
/* Location: ./application/helpers/MY_url_helper.php */