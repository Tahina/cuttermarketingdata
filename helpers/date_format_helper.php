<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('format_date'))
{
    /*
    Formats date according to target
    $target = form -> src: YYYY-mm-dd H:i:s dest: dd/mm/YYYY H:i:s
    $target = mysql -> src: dd/mm/YYYY H:i:s dest: YYYY-mm-dd H:i:s
    */
    function my_format_date($target = 'form', $datetime_string)
    {
        $datetime_arr = explode(' ', $datetime_string);
        if(isset($datetime_arr[0]) && isset($datetime_arr[1]))
        {        	
        	$date_str = $datetime_arr[0];
        	$time_str = $datetime_arr[1];
        	
        	if($target = 'form')
	        {
	        	// dest dd/mm/YYYY H:i:s
	        	$date_arr = explode('-', $date_str);
	        	$new_date = $date_arr[2] . '/' . $date_arr[1] . '/' . $date_arr[0];
	        	$new_datetime = $new_date . ' ' . $time_str;
	        } else {
	        	// dest YYYY-mm-dd H:i:s
	        	$date_arr = explode('/', $date_str);
	        	$new_date = $date_arr[2] . '/' . $date_arr[1] . '/' . $date_arr[0];
	        	$new_datetime = $new_date . ' ' . $time_str;
	        }

	        return $new_datetime;
        } else {
        	return false;
        }        
    }
}