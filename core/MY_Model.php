<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MY_model extends CI_Model
{
    /**
     * Create
     *
     * This function creates an entry based on $data_in
     *
     * @param   array    $escaped_data       A keyed array of criteria key = field name, value = value for escaped data
     * @param   array    $non_escaped_data   A keyed array of criteria key = field name, value = value for non escaped data
     * @access  public
     * @return  boolean  True if successful / False if not
     */
    public function create($escaped_data = array(), $non_escaped_data = array())
    {
     	$insert_id = FALSE;

        // Verify they input an array of data on insert
        if (is_array($escaped_data) && is_array($non_escaped_data)) 
        {
        	// Make sure they have already set the table
            if (empty($this->table)) 
            {                
                log_message('error', 'Model: Crud; Method: create($data_in); Required to select_table before using functions.');
                return FALSE;
            }

            // inssert
            $this->db->set($escaped_data)
	                               ->set($non_escaped_data, null, false)
	                               ->insert($this->table);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return  $insert_id;                       
        }
        else // param is not array
        {
            log_message('error', 'Model: Crud; Method: create($data_in); Paramater not set.');
            return FALSE;
        }
    }
     
    /**
	 * Update
	 *  
	 * @param   array    $criterea  WHERE
	 * @param   array    $escaped_data       A keyed array of criteria key = field name, value = value for escaped data
     * @param   array    $non_escaped_data   A keyed array of criteria key = field name, value = value for non escaped data
     * @access  public
	 * @return  bool     FASLE | TRUE on success or failure
	 */
    public function update($criteria = '', $escaped_data = array(), $non_escaped_data = array())
	{
	    if (is_array($criteria) && is_array($escaped_data) && is_array($non_escaped_data)) 
        {
		    $this->db->where($criteria);
            
            // Returns false on problem, or object on success.
            return (bool) $this->db->set($escaped_data)
                                   ->set($non_escaped_data, null, false)
                                   ->update($this->table);
		}
		else 
		{
			log_message('error', 'Model: Crud; Method: update ($criteria, $data_in); Required parameter not set.');
            return FALSE;
		}
	}
     
    /**
	 *  Delete
	 *  
	 * @param array  $criteria WHERE clause for selecting rows to delete
     * @access  public
	 * @return bool  FALSE|TRUE
	 */
	public function delete($criteria = array())
	{
	    if(is_array($criteria))
        {
            // Set the Criteria
        $this->db->where($criteria);

        return $this->db->where($criteria)
                        ->delete($this->table);
        }
        else
        {
            return FALSE;
        }    
	}
    
    /**
     * get_single
     *
     * retrives 1 row from clients depending on condition in $where
     *
     * @access  public
     * @param   array $where WHERE filed_name => field_value
     * @access  public
     * @return  Mixed   Return Integer of resutls on success, Boolean False on failure
     */
    public function get_single($where = array())
    {
    	$fields = '*';

        if(!is_array($where))
        {
            $where = array(
                    'id' => intval($where) // forces query to look for id
                );
        }
        $query = $this->retrieve($fields, $where, 1, 0); // only 1 row

        //echo $str = $this->db->last_query(); 

        if($query->num_rows() > 0)
        {
            $result = $query->row_object();            
            return $result;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * retrieve
     *
     * This function retrieves a series of db entries based on criteria.
     *
     * @param   array   $criteria       A keyed array of criteria key = field name, value = value, key may also contain comparators (=, !=, >, etc..)
     * @param   int     $limit          The max number of entries to grab (0 = no limit)
     * @param   int     $offset         What record number to start grabbing (useful for pagination)
     * @param   array   $order          A keyed array of "order commands" telling how to sort key = field name, value = direction (asc, desc, random)
     * @param   array   $order          A keyed array of like key = field name
     * @access  public
     * @return  mixed   Return Object of results on success, Boolean False on failure
     */
    public function retrieve($fields = "*", $criteria = '', $limit = 0, $offset = 0, $order = '', $like = '') 
    {
        
        // verify the table we are drawing from has been set.
        if (!empty($this->table)) 
        {
            $this->db->from($this->table);
        } 
        else 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: retrieve($criteria, $limit, $offset, $order); Required to select_table before using functions.');
            return FALSE;
        }

        $this->db->select($fields);

        // Verify an array has been passed.
        if (is_array($criteria)) 
        {

            if (is_array($order)) 
            {
                foreach ($order as $order_by => $direction) 
                {
                    $this->db->order_by($order_by, $direction);
                }
                unset($order_by, $direction);
            }

            if (!empty($limit)) 
            {
                if (!empty($offset)) 
                {
                    $this->db->limit($limit, $offset);
                } 
                else 
                {
                    $this->db->limit($limit);
                }
            }

            // like
            if(is_array($like))
            {
                $this->db->like($like);
            }

            $this->db->where($criteria);            
            $res = $this->db->get();
            //echo $this->db->last_query();
            return $res;
        } 
        else 
        {
            log_message('error', 'Model: Crud; Method: retrieve($criteria, $limit, $offset, $order); Required parameter not set.');
            return FALSE;
        }
    }

    /**
     * function count_all()
     *
     * This function simply counts ALL entries in the selected table.
     *
     * @access  public
     * @return  Mixed   Return Integer of resutls on success, Boolean False on failure
     */
    public function count_all() 
    {
        // verify the table we are drawing from has been set.
        if (empty($this->table)) 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: count(); Required to select_table before using functions.');
            return FALSE;
        }
        return $this->db->count_all($this->table);
    }

    /**
     * count_results
     *
     * This function simply counts ALL entries in the selected DB with a given criteria.
     *
     * @param   array   $criteria    A keyed array with the critera for selecting what entries to edit.
     * @access  public
     * @return  Mixed   Return Integer of resutls on success, Boolean False on failure
     */
    public function count_results($fields = '*', $criteria, $like = '') 
    {
        // verify the table we are drawing from has been set.
        if (empty($this->table)) 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: count_results($criteria); Required to select_table before using functions.');
            return FALSE;
        }
        // Select fields
        $this->db->select($fields);
        if(is_array($like))
        {
            $this->db->like($like);
        }
        $this->db->where($criteria);
        return $this->db->get($this->table)->num_rows();
        //return $this->db->count_all_results();
    }


    /**
     * get_last_id
     * 
     * Gets the last Id from a table          
     *
     * @access  public
     * @return  int   Returns an id
     *
     */
    function get_last_id ()
    {
        $last_id = 0;
        
        $this->db->select('id');
        $this->db->from($this->table);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1, 0);
        $query = $this->db->get();

        foreach ($query->result() as $row)
        {
            $last_id = $row->id;
        }
        
        return $last_id;
    }

    /**
     * is_entry_unique
     *
     * This function checks to see if an entry exists in the database matching the given criteria.
     *
     * @param   array   $criteria    A keyed array with the critera for selecting what entries to edit.
     * @access  public
     * @return  boolean Return Boolean TRUE if no match was found (aka it is unique). FALSE if a match is found (aka it is not unique)
     */
    public function is_entry_unique($criteria = '') 
    {        
        // verify the table we are drawing from has been set.
        if (empty($this->table)) 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: is_entry_unique($criteria); Required to select_table before using functions.');
            return FALSE;
        }

        $result = $this->retrieve($criteria);
        
        if ($result !== FALSE) 
        {
            if ($result->num_rows() > 0) 
            {
                return FALSE;
            } 
            else 
            {
                return TRUE;
            }
        } 
        else 
        {
            log_message('debug', 'Model: Crud; Method: is_entry_unique($criteria); Error in retrieving criteria.');
            return FALSE;
        }
    }
}

/* End of file MY_Model.php */
/* Location: ./system/application/core/MY_Model.php */